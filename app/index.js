// init application:
angular.module('app', [
    //vendors
    'ngResource',
    'ui.router',
    'ngMessages',
    'jlareau.pnotify',

    //templates
    'appTemplates',

    'components',
    'modules',
    'layout'
]);
