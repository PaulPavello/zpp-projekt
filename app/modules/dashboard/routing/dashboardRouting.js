angular.module('dashboard').config([
    '$stateProvider',
    function($stateProvider) {
        $stateProvider.state('dashboard', {
            parent: 'app',
            url: '/dashboard',
            views: {
                'content@': {
                    templateProvider: [
                        '$templateCache',
                        function($templateCache) {
                            return $templateCache.get('app/modules/dashboard/templates/dashboard.html');
                        }
                    ],
                    controller: 'dashboardController',
                    controllerAs: '_ctrl'
                }
            }
        });
    }
]);
