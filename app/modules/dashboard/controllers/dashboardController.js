angular.module('dashboard').controller('dashboardController', [
  'notificationService',
  'codesResolved',
    function (
      notificationService,
      codesResolved
    ) {
        this.model = {
            displayLimit: 3,
            reverse: true,
            orderBy: 'createDate',
            codes: codesResolved
        };

        this.displayMore = function() {
            if (this.model.displayLimit > this.model.codes.length) {
                return;
            }
            this.model.displayLimit+= 3;
        };

        this.setOrderBy = function(filterName) {
            this.model.orderBy = filterName;
            this.model.reverse= !this.model.reverse;
        };

        this.resetFilters = function() {
            this.model.orderBy = 'createDate';
            this.model.reverse = true;
        };
    }
]);
