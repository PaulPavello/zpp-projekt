angular.module('login').config([
    '$stateProvider',
    function($stateProvider) {
        $stateProvider.state('login', {
            parent: 'app',
            url: '/login',
            views: {
                'content@': {
                    templateProvider: [
                        '$templateCache',
                        function($templateCache) {
                            return $templateCache.get('app/modules/login/templates/login.html');
                        }
                    ],
                    controller: 'loginController',
                    controllerAs: '_ctrl'
                }
            }
        });
    }
]);
