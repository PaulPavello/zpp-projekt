angular.module('code').controller('previewController', [
    '$state',
    'codeResolved',
    'notificationService',
    function (
        $state,
        codeResolved,
        notificationService
    ) {
        this.model = {
            code: codeResolved
        };

    }
]);
