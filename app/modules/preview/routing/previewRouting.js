angular.module('preview').config([
    '$stateProvider',
    function($stateProvider) {

        $stateProvider.state('preview', {
           abstract: true,
           parent: 'app',
           url: '/preview'
       });
	   
        $stateProvider.state('preview.code', {
            url: '/:code',
            views: {
                'content@': {
                    templateProvider: [
                        '$templateCache',
                        function($templateCache) {
                            return $templateCache.get('app/modules/preview/templates/preview.html');
                        }
                    ],
                    controller: 'previewController',
                    controllerAs: '_ctrl'
                }
            },
            resolve: {
                codeResolved: [
                    '$stateParams',
                    'pastebinRepositories',
                    function(
                        $stateParams,
                        pastebinRepositories
                    ) {
                        return pastebinRepositories.getCode({id: $stateParams.code});
                    }
                ]
            }

        });
    }
]);
