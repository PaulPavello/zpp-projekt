angular.module('contact').config([
    '$stateProvider',
    function($stateProvider) {
        $stateProvider.state('contact', {
            parent: 'app',
            url: '/contact',
            views: {
                'content@': {
                    templateProvider: [
                        '$templateCache',
                        function($templateCache) {
                            return $templateCache.get('app/modules/contact/templates/contact.html');
                        }
                    ],
                    controller: 'contactController',
                    controllerAs: '_ctrl'
                }
            }
        });
    }
]);
