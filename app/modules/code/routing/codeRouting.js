angular.module('code').config([
    '$stateProvider',
    function($stateProvider) {
        $stateProvider.state('code', {
            parent: 'app',
            url: '/code',
            views: {
                'content@': {
                    templateProvider: [
                        '$templateCache',
                        function($templateCache) {
                            return $templateCache.get('app/modules/code/templates/code.html');
                        }
                    ],
                    controller: 'codeController',
                    controllerAs: '_ctrl'
                }
            }
        });
    }
]);
