angular.module('code').controller('codeController', [
    'notificationService',
    function (notificationService) {

        this.model = {

        };

        this.submit = function() {
            if(this.model.codeForm.$invalid) {
                notificationService.error('Nie udało sie dodać kodu!');
                return;
            }
            notificationService.success('Kod dodano');
        };
    }
]);
