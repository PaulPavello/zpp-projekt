angular.module('app').factory('dateUtils', [
    function () {
        function isDate(date) {
            return (
                !!date &&
                'object' === typeof date &&
                date instanceof Date &&
                !isNaN(date.getTime()) && //detect Invalid Date
                date.getTime() //detect default date (1970)
            );
        }

        function mergeDate(date, time) {
            if (!isDate(date)) {
                return null;
            }

            var mergedDate = new Date(date);
            var hours = 0;
            var minutes = 0;
            var milliseconds = 0;

            if (isDate(time)) {
                hours = time.getHours();
                minutes = time.getMinutes();
                milliseconds = time.getMilliseconds();
            }

            mergedDate.setHours(hours);
            mergedDate.setMinutes(minutes);
            mergedDate.setMilliseconds(milliseconds);

            return mergedDate;
        }

        function splitDate(mergedDate) {
            if (!isDate(mergedDate)) {
                return {
                    date: null,
                    time: null
                };
            }

            var date = new Date(mergedDate);
            date.setHours(0);
            date.setMinutes(0);
            date.setMilliseconds(0);

            var time = new Date(0);
            time.setHours(mergedDate.getHours());
            time.setMinutes(mergedDate.getMinutes());
            time.setMilliseconds(mergedDate.getMilliseconds());

            return {
                date: date,
                time: time
            };
        }

        function getStartDay(date) {
            var startDay = new Date(date);

            startDay.setHours(0);
            startDay.setMinutes(0);
            startDay.setSeconds(0);
            startDay.setMilliseconds(0);

            return startDay;
        }

        function getEndDay(date) {
            var endDay = new Date(date);

            endDay.setHours(23);
            endDay.setMinutes(59);
            endDay.setSeconds(59);
            endDay.setMilliseconds(999);

            return endDay;
        }

        return {
            isDate: isDate,
            mergeDate: mergeDate,
            splitDate: splitDate,
            getStartDay: getStartDay,
            getEndDay: getEndDay
        };
    }
]);
