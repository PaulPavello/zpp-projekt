angular.module('app').factory('objectUtils', [
    '$filter',
    function ($filter) {

        function isDate(value) {
            return (
                !!value &&
                'object' === typeof value &&
                value instanceof Date &&
                !isNaN(value.getTime()) && //detect Invalid Date
                value.getTime() //detect default date (1970)
            );
        }

        function isObject(value) {
            return angular.isObject(value) && !angular.isArray(value);
        }

        function parseBoolean(value) {
            if (typeof value === 'string') {
                value = value.toLowerCase();
            }
            try {
                value = !!JSON.parse(value);
            } catch(e) {
                value = false;
            }
            return value;
        }

        function parseValue(value, type) {
            if (value === undefined) {
                return value;
            }

            switch (type) {
                case 'date':
                    return (!!value && new Date(value)) || undefined;
                case 'float':
                    return parseFloat(value);
                case 'int':
                    if (isNaN(value)) {
                        return;
                    }
                    return parseInt(value);
                case 'boolean':
                    return parseBoolean(value);
                case 'string':
                    return value.toString();
            }

            return value;
        }

        function forceType(source, data) {
            var matcher = source.match(/^\[([a-z]+)(?:\|(.*))?\]$/);

            if (matcher) {
                var dataToParse = data;
                if (undefined === dataToParse) {
                    dataToParse = matcher[2];
                }
                return parseValue(dataToParse, matcher[1]);
            }

            return data;
        }

        function getValue(source, data) {
            if (angular.isArray(source)) {
                return data || source;
            }
            if (typeof source === 'string') {
                return forceType(source, data);
            }

            return data;
        }

        function fillObjectData(source, data) {

            angular.forEach(source, function (value, key) {
                if (isObject(value)) {
                    fillObjectData(value, data[key] || {});
                } else {
                    if(source.hasOwnProperty(key) && data.hasOwnProperty(key)) {
                        if(angular.isArray(value) && value.length) {
                            var arrayModel = value[0];
                            source[key] = [];
                            if (angular.isObject(arrayModel)) {
                                angular.forEach(data[key], function (arrayValue, arrayKey) {
                                    source[key][arrayKey] = fillObjectData(angular.copy(arrayModel), arrayValue);
                                });
                            } else {
                                source[key] = value;
                            }
                        } else {
                            source[key] = getValue(source[key], data[key]);
                        }
                    } else {
                        source[key] = getValue(source[key], data[key]);
                    }
                }
            });

            return source;
        }

        function prepareToSend(object, convertBoolToInt) {
            for(var key in object) {
                if (object[key] === undefined) {
                    delete object[key];
                } else if (isDate(object[key])) {
                    object[key] = $filter('date')(object[key], 'yyyy-MM-dd HH:mm');
                } else if (convertBoolToInt && typeof object[key] === 'boolean') {
                    object[key] = (object[key])?1:0;
                } else if(typeof object[key] === 'object') {
                    object[key] = prepareToSend(object[key], convertBoolToInt);

                    if (angular.isArray(object[key]) && !object[key].length) {
                        delete object[key];
                    } else if (angular.equals(object[key], {})) {
                        delete object[key];
                    }
                }
            }

            return object;
        }

        return {
            fillData: fillObjectData,
            prepareToSend: prepareToSend,
            getEmptyModel: function (model) {
                var outputModel = angular.copy(model);
                return fillObjectData(outputModel, {});
            }
        };
    }
]);
