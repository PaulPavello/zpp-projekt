angular.module('app').factory('pastebinRepositories', [
    'CONFIG',
    '$resource',
    function(
        CONFIG,
        $resource
    ) {
        var resource = $resource('', {},
            {
                getCode: {
                    method: 'GET',
                    url: CONFIG.apiUrl + '/code/:id'
                },
                addCode : {
                  method: 'POST',
                  url: CONFIG.apiUrl + '/code'
                }
            }
        );

        return resource;
    }
]);
