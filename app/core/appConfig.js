angular.module('app').config([
    '$stateProvider',
    '$locationProvider',
    '$urlRouterProvider',
    '$httpProvider',
    'CONFIG',
    function (
        $stateProvider,
        $locationProvider,
        $urlRouterProvider,
        $httpProvider,
        CONFIG
    ) {
        //requests
        $httpProvider.interceptors.push([
            '$q',
            '$rootScope',
            'langFactory',
            function (
                $q,
                $rootScope,
                langFactory
            ) {
                return {
                    request: function (config) {
                        config.timeout = CONFIG.requestTimeout;
                        config.headers['Accept-Language'] = langFactory.getLanguage();

                        return config;
                    },
                    responseError: function (response) {
                        $rootScope.$emit('http.response.error', response);
                        return $q.reject(response);
                    }
                };
            }
        ]);

        //routing

        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise('/dashboard');

        $stateProvider.state('app', {
            abstract: true,
            resolve: {
                resolvedTranslations: [
                    'langService',
                    function (langService) {
                        return langService.loadScripts();
                    }
                ]
            }
        });
    }
]);
