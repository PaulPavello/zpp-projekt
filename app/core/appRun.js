angular.module('app').run([
    '$rootScope',
    function ($rootScope) {
        $rootScope.$on('http.response.error', function (e, response) {

            switch (response.status) {
                case 0:
                case 400:
                case 401:
                case 405:
                case 500:
                case 404:
                    break;

                default:
                    break;
            }
        });

        $rootScope.$on('$locationChangeStart', function (event) {

        });

        $rootScope.$on('$stateChangeStart', function (event, toState) {

        });

        $rootScope.$on('$stateChangeSuccess', function (event, toState, toStateParams, fromState) {

        });

        $rootScope.$on('$stateChangeError', function () {

        });

        $rootScope.$on('$stateChangePermissionDenied', function () {

        });
    }
]);
