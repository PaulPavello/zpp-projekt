angular.module('local-store').factory('localStore', [
    'CONFIG',
    function (CONFIG) {

    var storage = window.localStorage;
    var localStore = {};

    localStore.prefixed = function (key) {
        return CONFIG.localstorePrefix + key;
    };

    localStore.has = function (key) {
        return !!storage.getItem(this.prefixed(key));
    };

    localStore.get = function (key) {
        try {
            return JSON.parse(storage.getItem(this.prefixed(key)));
        } catch(e) {
            return '';
        }
    };

    localStore.set = function (key, value) {
        storage.setItem(this.prefixed(key), JSON.stringify(value));
    };

    localStore.remove = function (key) {
        storage.removeItem(this.prefixed(key));
    };

    return localStore;

}]);
