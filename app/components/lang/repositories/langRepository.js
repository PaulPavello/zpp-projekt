angular.module('lang').factory('langRepository', [
    '$resource',
    function ($resource) {
        var resource = $resource(
            'locales/:lang',
            {
                lang: '@lang'
            }
        );

        return resource;
    }]
);
