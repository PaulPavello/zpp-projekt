angular.module('lang').factory('langFactory', [
    'localStore',
    'CONFIG',
    function (
        localStore,
        CONFIG
    ) {
        return {
            getLanguage: function () {
                return localStore.get('lang') || CONFIG.defaultLang;
            },
            setLanguage: function (lang) {
                return localStore.set('lang', lang);
            }
        };
    }
]);
