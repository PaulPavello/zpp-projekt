angular.module('lang').service('langService', [
    '$q',
    'langFactory',
    'langRepository',
    'LOCALES_PATH',
    function (
        $q,
        langFactory,
        langRepository,
        LOCALES_PATH
    ) {
        var service = {};
        var query;
        var langObject;

        service.get = function () {
            return langFactory.getLanguage();
        };

        service.set = function (lang) {
            return langFactory.setLanguage(lang);
        };

        service.loadScripts = function () {
            if (query) {
                return query.promise;
            }

            query = $q.defer();

            langRepository.get(
                {
                    lang: LOCALES_PATH[langFactory.getLanguage()]
                },
                function (response) {
                    langObject = response;
                    query.resolve();
                },
                function () {
                    query.reject();
                }
            );

            return query.promise;
        };

        service.trans = function (key, params) {
            if (!langObject || !langObject.hasOwnProperty(key)) {
                return key;
            }

            if (params) {
                return parseTranslation(langObject[key], params);
            }

            return langObject[key];
        };

        function parseTranslation(source, params) {
            angular.forEach(params, function (value, key) {
                var regex = new RegExp('{%\\s*' + key + '\\s*%}', 'g');
                source = source.replace(regex, value);
            });

            return source;
        }

        return service;
    }
]);
