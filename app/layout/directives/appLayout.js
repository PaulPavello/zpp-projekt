angular.module('layout').directive('appLayout', [
    '$rootScope',
    '$templateCache',
    '$state',
    function (
        $rootScope,
        $templateCache,
        $state
    ) {
        return {
           restrict: 'E',
           template: $templateCache.get('app/layout/templates/app-layout.html'),
           link: function (scope, element, attr) {
               scope.activeState = '';

               $rootScope.$on('$stateChangeSuccess', function (event, toState, toStateParams, fromState) {
                   scope.activeState = $state.current.name;
               });
           }
       };
    }
]);
