/**
 * define folders paths
 */

var paths = {
    setup: 'app/core/config/',
    styles: {
        dest: 'web/css/',
        main: {
            provider: {
                name: 'bourbon',
                location: 'src/scss/provider/',
                baseFile: 'tools/base.scss'
            },
            src: 'src/scss/layout/**/*.scss',
            sass: 'src/scss/layout/'
        },
        vendors: {
            src: 'src/vendors/',
            list: []
        }
    },

    scripts: {
        dest: 'web/scripts/',
        app: {
            src: 'app/**/*.js'
        },
        templates: {
            src: 'app/**/*.html'
        },

        vendors: {
            src: 'src/vendors/',
            list: []
        }
    },

    fonts: {
        src: 'web/assets/fonts/',
        dest: 'web/fonts'
    },

    images: {
        dest: 'web/images'
    },

    locales: {
        src: 'src/locales/',
        dest: 'web/locales/'
    },

    build: {
        templates: {
            index: 'app/layout/templates/index.html',
            cordova: 'app/layout/templates/index-cordova.html'
        },
        dest: 'web/',
        manifest: './rev-manifest.json'
    },

    test: {
        src: 'test/',
        list: []
    }
};

paths.scripts.vendors.list = [
    paths.scripts.vendors.src + 'angular/angular.js',
    paths.scripts.vendors.src + 'jquery/dist/jquery.min.js',
    paths.scripts.vendors.src + 'angular-ui-router/release/angular-ui-router.js',
    paths.scripts.vendors.src + 'angular-resource/angular-resource.min.js',
    paths.scripts.vendors.src + 'angular-messages/angular-messages.min.js',
    paths.scripts.vendors.src + 'jsSHA/src/sha1.js',
    paths.scripts.vendors.src + 'x2js/xml2json.min.js',
    paths.scripts.vendors.src + 'bootstrap/dist/js/bootstrap.min.js',
    paths.scripts.vendors.src + 'angular-pnotify/src/angular-pnotify.js',
    paths.scripts.vendors.src + 'pnotify/pnotify.core.js'
];

paths.styles.vendors.list = [
    paths.styles.vendors.src + 'bootstrap/dist/css/bootstrap.min.css',
    paths.styles.vendors.src + 'pnotify/pnotify.core.css'
];

paths.test.list = [
    paths.scripts.dest + 'vendors.js',
    paths.scripts.dest + 'templates.js',
    paths.scripts.dest + 'app.js',
    paths.test.src + '**/*.js'
];

module.exports = {
    server: {
        host: 'localhost',
        port: 8080,
        livereload: false
    },
    paths: paths,
    jshint: {
        "undef": true,
        "camelcase": false,
        "globals": {
            "window": false,
            "document": false,
            "console": false,
            "alert": false,
            "angular": true,
            "require": false,
            "module": false,
            "$": false,
            "jQuery": false,
            "X2JS": false
        }
    }
};
