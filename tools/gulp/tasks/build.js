module.exports = function (gulp, plugins, config) {

    /**
     * build index.html from rev-manifest paths
     */
    gulp.task('build-production', ['minify-and-rev'], function () {

        return gulp.src([
                config.paths.build.manifest,
                config.paths.build.templates.index
            ])
            .pipe(plugins.revCollector({ replaceReved: true }))
            .pipe(gulp.dest(config.paths.build.dest));
    });

    /**
     *  build index.html without rev-manifest, with plain paths
     */
    gulp.task('build-development', ['clean-rev', 'scripts', 'styles', 'server'], function () {
        return gulp.src(config.paths.build.templates.index)
            .pipe(gulp.dest(config.paths.build.dest));
    });

    gulp.task('build-cordova', ['clean-rev', 'scripts', 'styles'], function () {
        return gulp.src(config.paths.build.templates.cordova)
            .pipe(plugins.rename(function (path) {
                path.basename = 'index';
            }))
            .pipe(gulp.dest(config.paths.build.dest));
    });
};
