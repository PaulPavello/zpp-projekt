module.exports = function (gulp, plugins, config) {
    var sourcemaps = require('gulp-sourcemaps');
    gulp.task('scripts', ['scripts-app', 'scripts-templates', 'scripts-vendors']);

    /**
     * run js-hinter on concatenate scripts
     */
    gulp.task('scripts-app', ['scripts-app-concat'], function () {
        return gulp.src(config.paths.scripts.app.src)
            .pipe(plugins.jshint(config.jshint))
            .pipe(plugins.jshint.reporter('jshint-stylish'));
    });

    /**
     * concatenate app files
     */
    gulp.task('scripts-app-concat', function () {
        return gulp.src(config.paths.scripts.app.src)
            .pipe(sourcemaps.init())
            .pipe(plugins.concat('app.js'))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(config.paths.scripts.dest))
            ;
    });

    /**
     * concatenate templates files
     */
    gulp.task('scripts-templates', function () {
        return gulp.src(config.paths.scripts.templates.src)
            .pipe(plugins.minifyHtml({ empty: true, spare: true, quotes: true }))
            .pipe(plugins.html2js({
                outputModuleName: "appTemplates",
                useStrict: true
            }))
            .pipe(plugins.concat("templates.js"))
            .pipe(gulp.dest(config.paths.scripts.dest))
            ;
    });

    /**
     * concatenate vendors files
     */
    gulp.task('scripts-vendors', ['bower'], function () {
        return gulp.src(config.paths.scripts.vendors.list)
            .pipe(plugins.concat('vendors.js'))
            .pipe(gulp.dest(config.paths.scripts.dest));
    });
};
