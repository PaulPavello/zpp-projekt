module.exports = function (gulp, plugins, config) {

    var sass = require('gulp-sass');
    var fs = require('fs');
    var path = require('path');

    function normalizeDirectoryPath(path) {

        if (path.substr(-1) !== '/') {
            path += '/';
        }
        if (path.substr(-1) !== /\\/g) {
            path = path.replace(/\\/g, '/');
        }

        return path;
    }

    /**
     * main task to concatenate styles
     */
    gulp.task('styles', ['styles-provider', 'styles-main', 'styles-vendors', 'fonts-ui-grid', 'fonts']);

    /**
     * main stylesheets
     */

    gulp.task('styles-provider', function () {
        var providerConfig = config.paths.styles.main.provider;

        var providerDir = normalizeDirectoryPath(providerConfig.location);
        providerDir = normalizeDirectoryPath(providerDir + providerConfig.name);

        var providerBaseFile = providerDir + providerConfig.baseFile;
        var providerBaseFilePath = path.dirname(providerBaseFile);

        if (!fs.existsSync(providerDir)){
            throw Error(providerConfig.name + ' directory is required in ' + providerConfig.location);
        }

        var relativePath = path.relative(config.paths.styles.main.sass, providerBaseFilePath);
        relativePath = normalizeDirectoryPath(relativePath);

        return gulp.src(providerBaseFile)
            .pipe(plugins.replace(new RegExp('\{this\}', 'g'), relativePath))
            .pipe(gulp.dest(providerConfig.location));
    });

    gulp.task('styles-main', ['styles-provider'], function () {
        var providerName = config.paths.styles.main.provider.name;

        function logError(error) {
            console.log(error);
            this.emit('end');
        }

        if ('compass' === providerName) {

            var configFile = 'compass/config.rb';

            return gulp.src(config.paths.styles.main.src)
                .pipe(plugins.compass({
                    config_file: normalizeDirectoryPath(config.paths.styles.main.provider.location) + configFile,
                    comments: true,
                    css: config.paths.styles.dest,
                    sass: config.paths.styles.main.sass
                }))
                .on('error', logError)
                .pipe(gulp.dest(config.paths.styles.dest));

        }

        if ('bourbon' === providerName) {

            return gulp.src(config.paths.styles.main.src)
                .pipe(sass().on('error', logError))
                .pipe(gulp.dest(config.paths.styles.dest));

        }

        throw Error(providerName + ' is unknown');

    });

    /**
     * run tasks for vendors stylesheets
     */
    gulp.task('styles-vendors', [
            'fonts-bootstrap',
            'fonts-fa',
            'images-world-flags-sprite'
        ], function () {
            return gulp.src(config.paths.styles.vendors.list)
                .pipe(plugins.concat('vendors.css'))
                .pipe(gulp.dest(config.paths.styles.dest));
        }
    );

    gulp.task('fonts', function () {
        return gulp.src(config.paths.fonts.src + '/*')
            .pipe(gulp.dest(config.paths.fonts.dest));
    });

    /****************************************
     * vendors tasks
     */

    gulp.task('fonts-bootstrap', ['bower'], function () {
        return gulp.src(config.paths.styles.vendors.src + 'bootstrap/dist/fonts/*')
            .pipe(gulp.dest(config.paths.fonts.dest));
    });

    gulp.task('fonts-fa', ['bower'], function () {
        return gulp.src(config.paths.styles.vendors.src + 'font-awesome/fonts/*')
            .pipe(gulp.dest(config.paths.fonts.dest));
    });
    gulp.task('images-world-flags-sprite', ['bower'], function () {
        return gulp.src(config.paths.styles.vendors.src + 'world-flags-sprite/images/*')
            .pipe(gulp.dest(config.paths.images.dest));
    });

    gulp.task('fonts-ui-grid', ['bower'], function () {
        return gulp.src([
                config.paths.styles.vendors.src + 'angular-ui-grid/*.ttf',
                config.paths.styles.vendors.src + 'angular-ui-grid/*.woff',
                config.paths.styles.vendors.src + 'angular-ui-grid/*.svg'
            ])
            .pipe(gulp.dest(config.paths.styles.dest));
    });

};
