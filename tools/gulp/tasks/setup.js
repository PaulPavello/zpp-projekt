module.exports = function (gulp, plugins, config) {

    var q = require('q');
    var fs = require('fs');
    var clc = require('cli-color');
    var inquirer = require('inquirer');

    var configPath = config.paths.setup + 'config.js';
    var configDistPath = config.paths.setup + 'config.js.dist';

    var argv = getFlatObject(require('yargs').argv);
    var objectKeys = [];
    var emptyConst = '<empty>';

    function getFlatObject(object, flatObject, parentKey) {
        flatObject = flatObject || {};
        parentKey = parentKey || '';

        for (var key in object) {
            if (typeof object[key] === 'object') {
                getFlatObject(object[key], flatObject, parentKey + key + '.');
            } else {
                flatObject[parentKey + key] = object[key];
            }
        }

        return flatObject;
    }

    function getConfigLineFromObject(item) {
        var output = '\n' + item.leadingSpaces + item.key + ': ';
        if (item.isString) {
            output += "'";
        }
        output += item.value;
        if (item.isString) {
            output += "'";
        }
        if (item.hasComa) {
            output += ',';
        }
        return output;
    }

    function getNewConfig(items) {
        var configContent = fs.readFileSync(configDistPath, 'UTF-8');

        for (var index in items) {
            var item = items[index];
            configContent = configContent.replace(item.searchString, getConfigLineFromObject(item));
        }
        return configContent;
    }

    function parseConfigItem(str, bracket, spaces, key, value) {

        //check if is object close
        if (bracket) {
            objectKeys.pop();
            return {
                searchString: str
            };
        }

        //parse value
        var isString = false;
        var hasComa = false;
        var itemComment = '';

        value = value.replace(
            new RegExp('^(?:\\\'(.*)\\\'|"(.*)"|([0-9]*)|({.*})|(\\\[.*\\\]))(,)?\\\s*(?://(.*))?$'),
            function (str, value1, value2, value3, value4, value5, coma, comment) {
                if (coma) {
                    hasComa = true;
                }
                if (value1 !== undefined || value2 !== undefined) {
                    isString = true;
                }
                if (comment) {
                    itemComment = comment;
                }
                return value1 || value2 || value3 || value4 || value5 || '';
            }
        );

        //check if is new object declaration
        var isObjectKey = ('{' === value);
        if (isObjectKey) {
            objectKeys.push(key);
            return {
                searchString: str
            };
        }

        var keyPrefix = objectKeys.join('.');
        if (keyPrefix) {
            keyPrefix = keyPrefix + '.';
        }

        return {
            searchString: str,
            key: key,
            fullKey: keyPrefix + key,
            value: value,
            comment: itemComment,
            isString: isString,
            hasComa: hasComa,
            leadingSpaces: spaces
        };
    }

    function getItemByFullKey(itemsArray, fullKey) {
        for (var index in itemsArray) {
            if (itemsArray[index].fullKey === fullKey) {
                return itemsArray[index];
            }
        }

        return null;
    }

    function parseConfig(path) {
        var configContent = fs.readFileSync(path, 'UTF-8');
        var items = [];

        objectKeys = [];

        configContent.replace(
            new RegExp('\\n(?:\\s*(})\\s*|(\\s*)([a-z0-9]+)\:\\s*(.*))', 'ig'),
            function (str, bracket, spaces, key, value) {
                var itemObject = parseConfigItem(str, bracket, spaces, key, value);

                if (itemObject.fullKey) {
                    items.push(itemObject);
                }

                return str;
            }
        );

        return items;
    }

    function doPrompt(key, comment, defaultValue) {
        var deferred = q.defer();
        var message = key;

        if (comment) {
            message += clc.magenta(' [' + comment + ']');
        }

        inquirer.prompt([{
            type: 'input',
            message: message,
            default: defaultValue  || emptyConst,
            name: 'value'
        }], function (response) {
            var value = response.value;
            if (value === emptyConst) {
                value = '';
            }
            deferred.resolve(value);
        });

        return deferred.promise;
    }

    function askForValues(configDistItems, configItems, result) {
        var deferred = q.defer();
        result = result || [];

        if (configDistItems.length) {
            var item = configDistItems.shift();

            if (argv[item.fullKey] !== undefined) {
                item.value = argv[item.fullKey];
                result.push(item);

                askForValues(configDistItems, configItems, result).then(function (response) {
                    deferred.resolve(response);
                });

            } else {
                var defaultValue = item.value;
                var currentConfigItem = getItemByFullKey(configItems, item.fullKey);
                if (currentConfigItem) {
                    defaultValue = currentConfigItem.value;
                }

                doPrompt(item.fullKey, item.comment, defaultValue).then(function (value) {
                    item.value = value;
                    result.push(item);

                    askForValues(configDistItems, configItems, result).then(function (response) {
                        deferred.resolve(response);
                    });
                });
            }
        } else {
            deferred.resolve(result);
        }

        return deferred.promise;
    }

    function getOutput(result) {
        var output = 'gulp setup';
        var quotTemp = '<<QUOT>>';

        for (var index in result) {
            var item = result[index];

            output += ' --' + item.fullKey + '=';
            if (item.isString) {
                output += quotTemp;
            }
            output += item.value;
            if (item.isString) {
                output += quotTemp;
            }
        }

        output = output.replace(/'/g, '\\\'');
        output = output.replace(/"/g, '\\\"');
        output = output.replace(new RegExp(quotTemp, 'g'), '"');

        return output;
    }

    function buildConfig() {
        var deferred = q.defer();

        var configDistItems = parseConfig(configDistPath);
        var configItems = {};

        if (fs.existsSync(configPath)) {
            configItems = parseConfig(configPath);
        }

        askForValues(configDistItems, configItems).then(function (result) {

            fs.writeFileSync(configPath, getNewConfig(result));

            console.log('');
            console.log('');
            console.log(getOutput(result));
            console.log('');
            console.log('');

            deferred.resolve();
        });

        return deferred.promise;
    }

    gulp.task('setup', function () {
        return buildConfig();
    });

};
