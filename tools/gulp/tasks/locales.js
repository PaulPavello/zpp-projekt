module.exports = function (gulp, plugins, config) {

    var Q = require('q');
    var path = require('path');
    var fs = require('fs');
    var locales = require('./_locales.js')(config);
    var translationsFilemap = {};

    gulp.task('locales', ['locales-config', 'clean-locales']);

    gulp.task('locales-config', ['locales-generator'], function () {
        gulp.src(config.paths.setup)
            .pipe(plugins.ngConfig('app', {
                    constants: {
                        LOCALES_PATH: translationsFilemap
                    },
                    createModule: false
                })
            )
            .pipe(plugins.rename('locales.js'))
            .pipe(gulp.dest(config.paths.setup));
    });

    gulp.task('locales-generator', function () {
        var languages = locales.getLanguages();
        var tasks = languages.map(function (language) {
            var saveFileQuery = Q.defer();
            var translationObject = {};
            var translationsPartialsPath = path.join(config.paths.locales.src, language);

            if (!fs.existsSync(config.paths.locales.dest)){
                fs.mkdirSync(config.paths.locales.dest);
            }

            locales.getFiles(translationsPartialsPath).map(function (file) {
                var translationPartial = path.join(translationsPartialsPath, file);
                var content = fs.readFileSync(translationPartial, 'utf8');

                try {
                    var data = locales.prepareObject(file, content);
                } catch(e) {
                    locales.logError('JSON parse failed in ' + translationPartial);
                    return false;
                }
                translationObject = locales.mergeObjects(translationObject, data);
            });

            if ('{}' === JSON.stringify(translationObject)) {
                locales.logError(language + ' translations are empty!');
            }

            locales.saveTranslations(language, translationObject, function () {
                saveFileQuery.resolve();
            });

            return saveFileQuery.promise;
        });

        return Q.all(tasks).then(function () {
            translationsFilemap = locales.translationsFilemap;
        });
    });

};
