module.exports = function (gulp, plugins, config) {

    /**
     * Watch for changes in files
     */
    gulp.task('bower', function () {
        return plugins.bower({ cwd: './tools/bower' })
            .pipe(gulp.dest(config.paths.scripts.vendors.src));
    });

};
