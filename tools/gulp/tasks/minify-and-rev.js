module.exports = function (gulp, plugins, config) {

    gulp.task('minify-and-rev', ['clean-rev', 'styles-manifest']);

    gulp.task('styles-manifest', ['scripts-manifest'], function () {
        return gulp.src([
                config.paths.styles.dest + 'style.css',
                config.paths.styles.dest + 'vendors.css'
            ])
            .pipe(plugins.rev())
            .pipe(gulp.dest(config.paths.styles.dest))
            .pipe(plugins.rev.manifest( { base: './', merge: true} ) )
            .pipe(gulp.dest('./'));
    });

    gulp.task('scripts-manifest', ['minify-styles'], function () {
        return gulp.src([
                config.paths.scripts.dest + 'app.js',
                config.paths.scripts.dest + 'templates.js',
                config.paths.scripts.dest + 'vendors.js'
            ])
            .pipe(plugins.rev())
            .pipe(gulp.dest(config.paths.scripts.dest))
            .pipe(plugins.rev.manifest( { base: './', merge: true} ) )
            .pipe(gulp.dest('./'));
    });

    /**
     * minify styles and add versioning hash
     */
    gulp.task('minify-styles', ['minify-scripts'], function () {
        return gulp.src([
                config.paths.styles.dest + 'style.css',
                config.paths.styles.dest + 'vendors.css'
            ])
            .pipe(plugins.minifyCss())
            .pipe(gulp.dest(config.paths.styles.dest));
    });

    /**
     * minify scripts and add versioning hash
     */
    gulp.task('minify-scripts', ['styles', 'scripts'], function () {
        return gulp.src([
                config.paths.scripts.dest + 'app.js',
                config.paths.scripts.dest + 'templates.js',
                config.paths.scripts.dest + 'vendors.js'
            ])
            .pipe(plugins.uglify())
            .pipe(gulp.dest(config.paths.scripts.dest));
    });
};
