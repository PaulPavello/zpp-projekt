module.exports = function (gulp, plugins, config) {

    /**
     * Watch for changes in files
     */
    gulp.task('watch', function () {
        // Watch .js app files
        gulp.watch(config.paths.scripts.app.src, ['scripts-app'])
            .on('change', runLiveReload);
        // Watch .js vendors files
        gulp.watch(config.paths.scripts.vendors.src, ['scripts-vendors'])
            .on('change', runLiveReload);
        // Watch .js vendors files
        gulp.watch(config.paths.scripts.templates.src, ['scripts-templates'])
            .on('change', runLiveReload);
        // Watch .scss files
        gulp.watch(config.paths.styles.main.src, ['styles-main'])
            .on('change', runLiveReload);
        // Watch locales files
        gulp.watch(config.paths.locales.src + '**/*.json', ['locales'])
            .on('change', runLiveReload);
    });

    function runLiveReload(file) {
        if (config.server.livereload) {
            gulp.src(file.path).pipe(plugins.connect.reload());
        }
    }
};
