﻿module.exports = function (gulp, plugins, config) {
    gulp.task('test', ['scripts-app', 'scripts-vendors', 'scripts-templates'], function () {
        return gulp.src('./dummy')
            .pipe(plugins.karma({
                files: config.paths.test.list,
                configFile: './tools/karma/config.js',
                action: 'run'
            }))
            .on('error', function (err) {
                this.emit('end');
            });
    });
};
