module.exports = function (gulp, plugins, config) {

    gulp.task('server', function () {
        plugins.connect.server({
            root: config.paths.build.dest,
            host: config.server.host,
            port: config.server.port,
            fallback: config.paths.build.dest + 'index.html',
            livereload: config.server.livereload
        });
    });

};
