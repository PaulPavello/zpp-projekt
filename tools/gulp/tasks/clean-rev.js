module.exports = function (gulp, plugins, config) {

    var rimraf          = require('rimraf');
    var revOutdated     = require('gulp-rev-outdated');
    var path            = require('path');
    var through         = require('through2');

    function cleaner() {
        return through.obj(function (file, enc, cb){
            rimraf( path.resolve( (file.cwd || process.cwd()), file.path), function (err) {
                if (err) {
                    this.emit('error', new plugins.util.PluginError('Cleanup old files', err));
                }
                this.push(file);
                cb();
            }.bind(this));
        });
    }

    gulp.task('clean-rev', ['clean-rev-scripts', 'clean-rev-styles']);

    gulp.task('clean-rev-styles', ['clean-rev-scripts'], function () {
        return gulp.src([config.paths.styles.dest + '*'], {read: false})
            .pipe(revOutdated(1))
            .pipe(cleaner());
    });

    gulp.task('clean-rev-scripts', function () {
        return gulp.src([config.paths.scripts.dest + '*'], {read: false})
            .pipe(revOutdated(1))
            .pipe(cleaner());
    });

    gulp.task('clean-locales', function () {
        return gulp.src([config.paths.locales.dest + '*'], {read: false})
            .pipe(revOutdated(2))
            .pipe(cleaner());
    });
};
