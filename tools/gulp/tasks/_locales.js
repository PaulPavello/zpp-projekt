module.exports = function (config) {

    var fs = require('fs');
    var path = require('path');
    var gutil = require('gulp-util');
    var crypto = require('crypto');

    return  {
        translationsFilemap: {},
        getLanguages: function () {
            try {
                var languages = this.getFolders(config.paths.locales.src);

                if (!languages.length) {
                    throw 'Empty languages dir!';
                }

                return languages;
            } catch (e) {
                this.logError('Create languages directories first!');
                return false;
            }
        },

        logError: function (msg) {
            gutil.log(gutil.colors.red('Locales: ' + msg));
        },

        saveTranslations: function (language, translationObject, cb) {
            var self = this;
            var translationsContent = this.createTranslationKeys(translationObject);
            var hash = this.getHashFromTranslation(translationsContent);
            var localesFilename = language + '-' + hash + '.json';

            fs.writeFile(
                path.join(config.paths.locales.dest, localesFilename),
                translationsContent,
                function () {
                    self.translationsFilemap[language] = localesFilename;
                    cb();
                }
            );
        },

        getHashFromTranslation: function (content) {
            var md5sum = crypto.createHash('md5');
            return md5sum.update(content).digest('hex').substr(1, 8)
        },

        prepareObject: function (file, content) {
            var translations = JSON.parse(content);
            var data = {};

            if (this.shouldAddNamespace(file)) {
                var namespace = file.replace('.json', '');
                data[namespace] = translations;
            } else {
                data = translations;
            }

            return data;
        },

        shouldAddNamespace: function (file) {
            return '_' !== file.substring(0, 1);
        },

        mergeObjects: function (dest, data) {
            for (var key in data) {
                dest[key] = data[key];
            }
            return dest;
        },

        getFolders: function (dir) {
            return fs.readdirSync(dir).filter(function (file) {
                return fs.statSync(path.join(dir, file)).isDirectory();
            });
        },

        getFiles: function (dir) {
            return fs.readdirSync(dir).filter(function (file) {
                return !fs.statSync(path.join(dir, file)).isDirectory();
            });
        },

        createTranslationKeys: function (structure) {
            var translations = {};

            function parseObjectStructure(structure, languageKey) {
                for (var key in structure) {
                    if ('string' === typeof structure[key]) {
                        translations[languageKey + key] = structure[key];
                    } else {
                        parseObjectStructure(structure[key], languageKey + key + '.');
                    }
                }
            }

            parseObjectStructure(structure, '');

            return JSON.stringify(translations);
        }
    };

};
