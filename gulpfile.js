/** load required plugins **/
var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    config = require('./tools/gulp/config.js');

require('./tools/gulp/tasks/setup.js')(gulp, plugins, config);
require('./tools/gulp/tasks/bower.js')(gulp, plugins, config);
require('./tools/gulp/tasks/server.js')(gulp, plugins, config);
require('./tools/gulp/tasks/styles.js')(gulp, plugins, config);
require('./tools/gulp/tasks/scripts.js')(gulp, plugins, config);
require('./tools/gulp/tasks/locales.js')(gulp, plugins, config);
require('./tools/gulp/tasks/clean-rev.js')(gulp, plugins, config);
require('./tools/gulp/tasks/minify-and-rev.js')(gulp, plugins, config);
require('./tools/gulp/tasks/build.js')(gulp, plugins, config);
require('./tools/gulp/tasks/test.js')(gulp, plugins, config);
require('./tools/gulp/tasks/watch.js')(gulp, plugins, config);

gulp.task('default', ['build-development', 'locales', 'watch']);
gulp.task('production', ['build-production', 'locales']);
gulp.task('cordova', ['build-cordova', 'locales']);
